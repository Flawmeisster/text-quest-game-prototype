﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestPlayer.Models
{
    public class Parameter
    {
        public int ParameterID { get; set; }
        public string ParameterName { get; set; }
        public int Value { get; set; }
    }
}
