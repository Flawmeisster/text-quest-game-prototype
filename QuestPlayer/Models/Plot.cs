﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestPlayer.Models
{
   public class Plot
    {
       public int PlotID { get; set; }
       public string Text { get; set; }
       public string ImageAdress { get; set; }
       public int TransitionID { get; set; }

    }
}
