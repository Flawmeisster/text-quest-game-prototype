﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestPlayer.Models
{
   public class Transition
    {
       public int TransitionID { get; set; }
       public string TransitionName { get; set; }
       public string TransitionText { get; set; }

    }
}
