﻿using System.Collections.Generic;
using System;
using System.Drawing;

// Типы данных игры
namespace DataTypes
{
	// Перечисление всевозможных операций над игровыми переменными
	public enum Operation { set, add, subtract, multiply, divide }
	// Перечисление всевозможных операторов сравнения значений игровых переменных
	public enum Comparator { equal, notEqual, less, more, lessOrEqual, moreOrEqual }
	// Интерфейс значения
	abstract public class Value
	{
		// Численное значение
		public class NumericValue : Value
		{
			public NumericValue(int val)
			{
				Val = val;
			}
			private int Val;
			override public int GetValue()
			{
				return Val;
			}
		}
		// Случайное значение
		public class RandomValue : Value
		{
			public RandomValue()
			{
			}
			override public int GetValue()
			{
				return rand.Next(10);
			}
		}
		static Random rand = new Random();
		public static Value MakeValue(string value)
		{
			if (value == "RAND")
				return new RandomValue();
			else
				return new NumericValue(Convert.ToInt32(value));
		}
		abstract public int GetValue();

	}
	// Условие осуществимости перехода
	public class Condition
	{
		public Condition(string parameter, Comparator sign, Value value)
		{
			Parameter = parameter;
			Sign = sign;
			Value = value;
		}
		// Имя проверяемого параметра
		public string Parameter { get; private set; }
		// Оператор сравнения
		public Comparator Sign { get; private set; }
		// Значение, с которым будет сравниваться параметр
		public Value Value { get; private set; }
	}
	// Изменения, совершаемые с игровыми переменными при выборе соответствующего перехода
	public class Change
	{
		public Change(string parameter, Operation sign, Value argument)
		{
			Parameter = parameter;
			Sign = sign;
			Argument = argument;
		}
		// Имя изменяемого параметра
		public string Parameter { get; private set; }
		// Арифметическая операция, которая будет над ним произведена
		public Operation Sign { get; private set; }
		// Значение, на которое будет изменён параметр
		public Value Argument { get; private set; }
	}
	// Переход от одного игрового события к другому
	public class Transition
	{
		public Transition(string destination, string description, IEnumerable<Condition> conditions, IEnumerable<Change> changes)
		{
			Destination = destination;
			Description = description;
			Conditions = new List<Condition>(conditions);
			Changes = new List<Change>(changes);
		}
		// Пункт назначения
		public string Destination { get; private set; }
		// Описание перехода для выбора в меню
		public string Description { get; private set; }
		// Условия перехода
		public List<Condition> Conditions { get; private set; }
		// Последующие за переходом изменения
		public List<Change> Changes { get; private set; }
	}
	// Событие, описывающее ситуации, в которые попадает игрок
	public class Event
	{
		public Event(string description, string image, IEnumerable<string> transitions)
		{
			Description = description;
			Image = image;
			Transitions = new List<string>(transitions);
		}
		// Описание ситуации
		public string Description { get; private set; }
		// Название картинки, содержащей визуальное изображение ситуации
		public string Image { get; private set; }
		// Возможные выходы из ситуации
		public List<string> Transitions { get; private set; }
	}

	public class DataManager
	{
		public DataManager(Parser.Script parsedScript)
		{
			InitialEvent = parsedScript.Game.InitialEvent;
			currentEventName = InitialEvent;

			Values = new Dictionary<string, int>();
			foreach (var p in parsedScript.Game.Parameters)
				Values[p.Parameter] = p.Argument.GetValue();

			Events = new Dictionary<string, DataTypes.Event>();
			foreach (var e in parsedScript.Events)
				Events[e.Name] = new DataTypes.Event(e.Description, e.Image, e.Transitions);

			Transitions = new Dictionary<string, DataTypes.Transition>();
			foreach (var t in parsedScript.Transitions)
				Transitions[t.Name] = new DataTypes.Transition(t.Destination, t.Description, t.Conditions, t.Changes);

			DisplayList = new Dictionary<string, string>(parsedScript.Game.ParameterDescriptions);
		}
		public bool TestCondition(Condition c)
		{
			if (Values.ContainsKey(c.Parameter))
			{
				int parameterValue = Values[c.Parameter];

				int testValue = c.Value.GetValue();
				switch (c.Sign)
				{
					case Comparator.equal:
						return parameterValue == testValue;
					case Comparator.less:
						return parameterValue < testValue;
					case Comparator.lessOrEqual:
						return parameterValue <= testValue;
					case Comparator.more:
						return parameterValue > testValue;
					case Comparator.moreOrEqual:
						return parameterValue >= testValue;
					case Comparator.notEqual:
						return parameterValue != testValue;
					default:
						return false;
				}
			}
			else
				throw new Exception("No such parameter in data manager.");
		}

		public void ApplyChange(Change c)
		{
			if (!Values.ContainsKey(c.Parameter))
				throw new Exception("No such parameter in data manager.");
			int parameterValue = Values[c.Parameter];
			int argumentValue = c.Argument.GetValue();
			switch (c.Sign)
			{
				case Operation.set:
					parameterValue = argumentValue;
					break;
				case Operation.add:
					parameterValue += argumentValue;
					break;
				case Operation.subtract:
					parameterValue -= argumentValue;
					break;
				case Operation.multiply:
					parameterValue *= argumentValue;
					break;
				case Operation.divide:
					parameterValue /= argumentValue;
					break;
			}
			Values[c.Parameter] = parameterValue;
		}

		public void MakeTransition(string transition)
		{
			var t = Transitions[transition];
			foreach (var change in t.Changes)
				ApplyChange(change);
			currentEventName = t.Destination;
		}

		public string InitialEvent { get; private set; }
		public Dictionary<string, int> Values { get; private set; }
		public Dictionary<string, string> DisplayList { get; private set; }
		public Dictionary<string, DataTypes.Event> Events { get; private set; }
		public Dictionary<string, DataTypes.Transition> Transitions { get; private set; }
		private string currentEventName;
		public Event CurrentEvent
		{
			get
			{
				return Events[currentEventName];
			}
		}
	}
}