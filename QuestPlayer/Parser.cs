﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Sprache;

// Парсер файла игрового скрипта
namespace Parser
{
	// Классы вспомогательных временных объектов
	public class Event
	{
		public Event(string name, string description, string image, IEnumerable<string> transitions)
		{
			Name = name;
			Description = description;
			Image = image;
			Transitions = new List<string>(transitions);
		}
		public string Name { get; private set; }
		public string Description { get; private set; }
		public string Image { get; private set; }
		public List<string> Transitions { get; private set; }
	}
	public class Transition
	{
		public Transition(string name, string destination, string description, IEnumerable<DataTypes.Condition> conditions, IEnumerable<DataTypes.Change> changes)
		{
			Name = name;
			Destination = destination;
			Description = description;
			Conditions = new List<DataTypes.Condition>(conditions);
			Changes = new List<DataTypes.Change>(changes);
		}
		public string Name { get; private set; }
		public string Destination { get; private set; }
		public string Description { get; private set; }
		public List<DataTypes.Condition> Conditions { get; private set; }
		public List<DataTypes.Change> Changes { get; private set; }
	}
	public class Game
	{
		public Game(string title, string initialevent, IEnumerable<DataTypes.Change> parameters, IEnumerable<KeyValuePair<string, string>> parameterDescriptions)
		{
			Title = title;
			InitialEvent = initialevent;
			Parameters = new List<DataTypes.Change>(parameters);
			ParameterDescriptions = new Dictionary<string,string>();
			foreach(var d in parameterDescriptions)
				ParameterDescriptions[d.Key] = d.Value;
		}
		public string Title { get; private set; }
		public string InitialEvent { get; private set; }
		public List<DataTypes.Change> Parameters { get; private set; }
		public Dictionary<string, string> ParameterDescriptions { get; private set; }
	}
	public class Script
	{
		public Script(Parser.Game game, IEnumerable<Parser.Event> events, IEnumerable<Parser.Transition> transitions)
		{
			Game = game;
			Events = new List<Parser.Event>(events);
			Transitions = new List<Parser.Transition>(transitions);
		}
		public Parser.Game Game { get; private set; }
		public List<Parser.Event> Events { get; private set; }
		public List<Parser.Transition> Transitions { get; private set; }
	}
	// Парсер скрипта, основанный на библиотеке Sprache, описывает синтаксис скрипта в виде дерева грамматик
	class ScriptParser
	{
		// Грамматики
		// Идентификатор - имя события, перехода или параметра
		// <Identifier> ::= <Char> [<CharSequence>]
		// <Char> ::= <Letter> | <Number>
		// <CharSequence> ::= <Char> <CharSequence> | ""
		private static readonly Parser<string> Identifier =
		(from identifier in Parse.LetterOrDigit.Many().Text()
		 select identifier).Token();
		// Открывающая фигурная скобка
		private static readonly Parser<char> LBracket =
		(from lbracket in Parse.Char('{')
		 select lbracket).Token();
		// Закрывающая фигурная скобка
		private static readonly Parser<char> RBracket =
		(from rbracket in Parse.Char('}')
		 select rbracket).Token();
		// Знак равенства
		private static readonly Parser<char> Equal =
		(from eq in Parse.Char('=')
		 select eq).Token();
		// Текст в кавычках
		private static readonly Parser<string> QuotedText =
		(from open in Parse.Char('"')
		 from content in Parse.CharExcept('"').Many().Text()
		 from close in Parse.Char('"')
		 select content).Token();
		// Текстовое описание
		// <Description> ::= "Description" <Equal> <QuotedText>
		private static readonly Parser<string> Description =
		(from descriptionTitle in Parse.String("Description")
		 from eq in Equal
		 from description in QuotedText
		 select description).Token();
		// Значение
		// <Value> ::= <Identifier> <Equal> <Val>
		// <Val> ::= <Number> | "RAND"
		private static readonly Parser<string> Value =
		 (from value in (Parse.Number.Or(Parse.String("RAND"))).Text()
		  select value).Token();
		// Условие
		// <Condition> ::= <Identifier> <Comparator> <Number>
		// <Comparator> ::= "==" | "!=" | ">=" | "<=" | ">" | "<"
		private static readonly Parser<DataTypes.Condition> Condition =
		(from name in Identifier
		 from operation in (Parse.String("==").Return(DataTypes.Comparator.equal).
		 Or(Parse.String("!=").Return(DataTypes.Comparator.notEqual)).
		 Or(Parse.String(">=").Return(DataTypes.Comparator.moreOrEqual)).
		 Or(Parse.String("<=").Return(DataTypes.Comparator.lessOrEqual)).
		 Or(Parse.String(">").Return(DataTypes.Comparator.more)).
		 Or(Parse.String("<").Return(DataTypes.Comparator.less))).Token()
		 from value in Value
		 select new DataTypes.Condition(name, operation, DataTypes.Value.MakeValue(value))).Token();
		// Изменение
		// <Change> ::= <Identifier> <Operation> <Number>
		// <Operation> ::= "=" | "+" | "-" | "*" | "/"
		private static readonly Parser<DataTypes.Change> Change =
		(from name in Identifier
		 from operation in (Parse.String("=").Return(DataTypes.Operation.set).
		 Or(Parse.String("+").Return(DataTypes.Operation.add)).
		 Or(Parse.String("-").Return(DataTypes.Operation.subtract)).
		 Or(Parse.String("*").Return(DataTypes.Operation.multiply)).
		 Or(Parse.String("/").Return(DataTypes.Operation.divide))).Token()
		 from value in Value
		 select new DataTypes.Change(name, operation, DataTypes.Value.MakeValue(value))).Token();
		// Изображение для события
		// <Image> ::= "Image" <Equal> <QuotedText>
		private static readonly Parser<string> Image =
		(from imageTitle in Parse.String("Image").Text()
		 from eq in Equal
		 from imageName in QuotedText
		 select imageName).Token();
		// Список переходов
		// <Transitions> ::= "TransitionList" <Equal> <TransitionList>
		// <TransitionList> ::= <Identifier> <TransitionList> | ""
		private static readonly Parser<IEnumerable<string>> TransitionList =
		(from transitionsTitle in Parse.String("TransitionList")
		 from eq in Equal
		 from transitions in Identifier.Many()
		 select transitions).Token();
		// Событие
		// <Event> ::= <Identifier>
		//             "{"
		//             <Description>
		//             <Image>
		//             <Transitions>
		//             "}"
		private static readonly Parser<Parser.Event> Event =
			(from name in Identifier
			 from lbracket in LBracket
			 from description in Description
			 from image in Image
			 from transitions in TransitionList
			 from rbracket in RBracket
			 select new Parser.Event(name, description, image, transitions)).Token();
		// Переход
		// <Transition> ::= <Identifier>
		//                  "{"
		//                  <Description>
		//                  <Destination>
		//                  <Conditions>
		//                  <Changes>
		//                  "}"
		// <Destination> ::= "Destination" <Equal> <Identifier>
		private static readonly Parser<Parser.Transition> Transition =
			(from name in Identifier
			 from lbracket in LBracket
			 from description in Description
			 from destinationTitle in Parse.String("Destination").Token()
			 from eq in Equal
			 from destination in Identifier
			 from conditions in Conditions
			 from changes in Changes
			 from rbracket in RBracket
			 select new Parser.Transition(name, destination, description, conditions, changes)).Token();
		// Условия
		// <Conditions> ::= "Conditions"
		//                  "{"
		//                  <ConditionList>
		//                  "}"
		// <ConditionList> ::= <Condition> <ConditionList> | ""
		private static readonly Parser<IEnumerable<DataTypes.Condition>> Conditions =
			(from sectionTitle in Parse.String("Conditions").Token()
			 from lbracket in LBracket
			 from conditions in Condition.Many()
			 from rbracket in RBracket
			 select conditions).Token();
		// Изменения
		// <Changes> ::= "Changes"
		//               "{"
		//               <ChangeList>
		//               "}"
		// <ChangeList> ::= <Change> <ChangeList> | ""
		private static readonly Parser<IEnumerable<DataTypes.Change>> Changes =
			(from sectionTitle in Parse.String("Changes").Token()
			 from lbracket in LBracket
			 from changes in Change.Many()
			 from rbracket in RBracket
			 select changes).Token();
		// Исходные параметры
		// Для упрощения кода сделано допущение, что добавляемый параметр описывается так же, как изменение,
		// при этом предполагается использование в скрипте на месте оператора знака "="
		// <Parameters> ::= "Parameters"
		//                  "{"
		//                  <ChangeList>
		//                  "}"
		private static readonly Parser<IEnumerable<DataTypes.Change>> Parameters =
			(from propertiesTitle in Parse.String("Parameters")
			 from lbracket in LBracket
			 from parameters in Change.Many()
			 from rbracket in RBracket
			 select parameters).Token();
		// Исходные параметры игры
		// <Game> ::= "Game"
		//            "{"
		//            <QuestTitle>
		//            <InitialEvent>
		//            <Parameters>
		//            <DisplayedParameters>
		//            "}"
		// <QuestTitle> ::= "QuestTitle" <Equal> <QuotedString>
		// <InitialEvent> ::= "InitialEvent" <Equal> <Identifier>
		private static readonly Parser<Parser.Game> Game =
			(from sectionTitle in Parse.String("Game")
			 from lbracket in LBracket
			 from gameTitle in Parse.String("QuestTitle").Token()
			 from eq1 in Equal
			 from title in QuotedText
			 from initialEventTitle in Parse.String("InitialEvent").Token()
			 from eq2 in Equal
			 from initialEvent in Identifier
			 from parameters in Parameters
			 from displayList in DisplayedParameters
			 from rbracket in RBracket
			 select new Parser.Game(title, initialEvent, parameters, displayList)).Token();
		// Описание отображаемого параметра
		// <ParameterDescription> ::= <Identifier> <Equal> <QuotedText>
		private static readonly Parser<KeyValuePair<string, string>> ParameterDescription =
			(from name in Identifier
			 from eq in Equal
			 from descr in QuotedText
			 select new KeyValuePair<string, string>(name, descr)).Token();
		private static readonly Parser<IEnumerable<KeyValuePair<string, string>>> DisplayedParameters =
			(from sectionTitle in Parse.String("DisplayedParameters")
			 from lbracket in LBracket
			 from parameter in ParameterDescription.Many()
			 from rbracket in RBracket
			 select parameter).Token();
		// События
		// <EventsSection> ::= "Events"
		//                     "{"
		//                     <EventsList>
		//                     "}"
		// <EventsList> ::= <Event> <EventsList> | ""
		private static readonly Parser<IEnumerable<Parser.Event>> Events =
			(from sectionTitle in Parse.String("Events")
			 from lbracket in LBracket
			 from events in Event.Many()
			 from rbracket in RBracket
			 select events).Token();
		// Переходы
		// <TransitionsSection> ::= "Transitions"
		//                          "{"
		//                          <TransitionsList>
		//                          "}"
		// <TransitionsList> ::= <Transition> <TransitionsList> | ""
		private static readonly Parser<IEnumerable<Parser.Transition>> Transitions =
			(from sectionTitle in Parse.String("Transitions")
			 from lbracket in LBracket
			 from transitions in Transition.Many()
			 from rbracket in RBracket
			 select transitions).Token();
		// Исходный скрипт
		// <Script> ::= <Game> <EventsSection> <TransitionsSection>
		public static readonly Parser<Parser.Script> Script =
			from game in Game
			from events in Events
			from transitions in Transitions
			select new Parser.Script(game, events, transitions);
	}
}