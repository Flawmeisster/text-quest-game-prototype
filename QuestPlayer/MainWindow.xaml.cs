﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sprache;

namespace QuestPlayer
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			availableTransitions = new List<string>();

			// Чтение данных из файла
			StreamReader reader = new StreamReader("../../GameData/prison.script");
			var input = reader.ReadToEnd();
			reader.Close();
			parsedScript = Parser.ScriptParser.Script.Parse(input);
			this.Title = parsedScript.Game.Title;

			// Инициализация исходных параметров
			dm = new DataTypes.DataManager(parsedScript);

			RefreshInterface();
		}

		// Реакция на выбор игрока
		public void MakeDecision(object sender, System.EventArgs e)
		{
			int selectedIndex = ChoiceList.SelectedIndex;
			if (selectedIndex < 0) // выбран неактивный элемент
				return;
			var selectedTransition = availableTransitions[selectedIndex];
			switch (selectedTransition)
			{
				case "EXIT":
					if (AskForExit())
						this.Close();
					break;
				case "RETRY":
					if (AskForRetry())
					{
						StartNewGame();
						return;
					}
					else
						this.Close();
					break;
				case "SAVE":
					if(AskForSave())
					{
						SaveCurrentSatistics();
					}
					if (AskForRetry())
					{
						StartNewGame();
					}
					else
						this.Close();
					break;
				default:
					{
						dm.MakeTransition(selectedTransition);
						RefreshInterface();
						break;
					}
			}
		}

		private void RefreshInterface()
		{
			DescriptionView.Text = dm.CurrentEvent.Description;

			StatusBox.Items.Clear();
			foreach (var d in dm.DisplayList)
			{
				var item = String.Format("{0}: {1}", d.Value, dm.Values[d.Key]);
				StatusBox.Items.Add(item);
			}

			string imageName = dm.CurrentEvent.Image;
			if (imageName != "")
			{
				var imagePath = String.Format(@"..\..\GameData\{0}.png", dm.CurrentEvent.Image);
				var fileInfo = new FileInfo(imagePath);
				var uri = new Uri(fileInfo.FullName);
				ImageView.Source = new BitmapImage(uri);
			}

			ChoiceList.Items.Clear();
			availableTransitions.Clear();

			int index = 0;
			foreach (var transitionName in dm.CurrentEvent.Transitions)
			{
				switch (transitionName)
				{
					case "EXIT":
						ChoiceList.Items.Add("Выйти");
						availableTransitions.Add(transitionName);
						break;
					case "RETRY":
						ChoiceList.Items.Add("Попытаться ещё");
						availableTransitions.Add(transitionName);
						break;
					case "SAVE":
						ChoiceList.Items.Add("Сохранить результаты");
						availableTransitions.Add(transitionName);
						break;
					default:
						{
							var transition = dm.Transitions[transitionName];
							bool canHappen = true;
							foreach (var condition in transition.Conditions)
								canHappen &= dm.TestCondition(condition);
							if (canHappen)
							{
								ChoiceList.Items.Add(dm.Transitions[transitionName].Description);
								availableTransitions.Add(transitionName);
							}
							break;
						}
				}
				index++;
			}
		}

		private bool AskForExit()
		{
			string message = "Выйти из игры?";
			string caption = "Выход";
			MessageBoxButton buttons = MessageBoxButton.YesNo;
			MessageBoxImage icon = MessageBoxImage.Question;
			MessageBoxResult defaultResult = MessageBoxResult.Yes;
			MessageBoxResult result = MessageBox.Show(message, caption, buttons, icon, defaultResult);

			return (result == MessageBoxResult.Yes) ? true : false;
		}

		private bool AskForRetry()
		{
			string message = "Попытаться ещё раз?";
			string caption = "Начать заново";
			MessageBoxButton buttons = MessageBoxButton.YesNo;
			MessageBoxImage icon = MessageBoxImage.Question;
			MessageBoxResult defaultResult = MessageBoxResult.Yes;
			MessageBoxResult result = MessageBox.Show(message, caption, buttons, icon, defaultResult);

			return (result == MessageBoxResult.Yes) ? true : false;
		}

		private bool AskForSave()
		{
			string message = "Сохранить результаты игры?";
			string caption = "Сохранение";
			MessageBoxButton buttons = MessageBoxButton.YesNo;
			MessageBoxImage icon = MessageBoxImage.Question;
			MessageBoxResult defaultResult = MessageBoxResult.Yes;
			MessageBoxResult result = MessageBox.Show(message, caption, buttons, icon, defaultResult);

			return (result == MessageBoxResult.Yes) ? true : false;
		}

		private void StartNewGame()
		{
			dm = new DataTypes.DataManager(parsedScript);
			RefreshInterface();
		}

		private void SaveCurrentSatistics()
		{
            var time = String.Format("{0:MM_dd_yy H_mm_ss}", DateTime.Now);
            var fileName = String.Format("{0} - {1}.txt", parsedScript.Game.Title, time);
			var writer = new StreamWriter(fileName);
			foreach(var d in dm.DisplayList)
			{
				var line = String.Format("{0}: {1}", d.Value, dm.Values[d.Key]);
				writer.WriteLine(line);
			}
		}

		private DataTypes.DataManager dm;
		private Parser.Script parsedScript;
		private List<string> availableTransitions;

        private void restart_Click(object sender, RoutedEventArgs e)
        {
            StartNewGame();

        }

        private void exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Stylespace_Click(object sender, RoutedEventArgs e)
        {
            DescriptionView.Foreground = Brushes.Ivory;
            ChoiceList.Foreground = Brushes.Ivory;
            StatusBox.Foreground = Brushes.Ivory;
            ImageBrush ib = new ImageBrush();
            ib.ImageSource = new BitmapImage(new Uri("D:/back1.jpg", UriKind.RelativeOrAbsolute));
            windoow.Background = ib;
        }

        private void Stylesun_Click(object sender, RoutedEventArgs e)
        {
            DescriptionView.Foreground = Brushes.SpringGreen;
            ChoiceList.Foreground = Brushes.SpringGreen;
            StatusBox.Foreground = Brushes.SpringGreen;
            ImageBrush brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("D:/back3.jpg", UriKind.RelativeOrAbsolute));
            windoow.Background = brush;
        }

        private void Styleclassic_Click(object sender, RoutedEventArgs e)
        {
            DescriptionView.Foreground = Brushes.Black;
            windoow.Background = Brushes.White;
            ChoiceList.Foreground = Brushes.Black;
            StatusBox.Foreground = Brushes.Black;
        }

        
	}
}
